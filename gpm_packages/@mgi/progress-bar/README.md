Writing _good_ progress bars is hard. How do we split up the progress percentage between tasks? How do we keep the user updated with whats going on?

The **Progress Bar API** provides a framework for developing good progress bars that your users can actually depend on. It provides easy ways for your code to update the status as well feedback mechanisms to allow common operations like aborting.

## Usage

The API is split into two main components: The Reader and the Writer. Abstracting these two pieces lets you easily separate your UI from your business logic. The UI is responsible for initiating the progress bar and the writer is responsible for updating the current state.

### The Reader

The reader classes initialize the progress bar API. It provides the progress consumer with helpful events and status of the progress bar. It contains all of the user events and interfaces to get information about the the current progress.

### The Writer

The writer is responsible for calculating the current progress bar percentage. This can be an awkward number to calculate. Often you have to break the percentage up into a few different sections and each section needs to be updated in a different way. This is what the writer classes enable you to do.

The progress is broken up into any number of weighted blocks. Blocks can be made up of concrete percentage calculations, or even be further divided into more blocks. Each block has the ability to update it's current progress, and the total process's progress bar is the weighted sum of all of it's blocks.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
