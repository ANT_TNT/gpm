# GPM Package File Parser
#### *This package is implemented with LabVIEW 2017*

This package contains the data wrappers to propery parse a `gpackage.json` file.

The `gpackage.json` file is used in the G Package Manager to store meta data about the package.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.
